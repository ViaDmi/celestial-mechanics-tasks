#pragma once

typedef struct _vector_3d
{
    double x, y, z;
} vector;

double get_dot_product(vector *a, vector *b);
double get_vector_length(vector *a);
double get_angle(vector *a, vector *b);
vector scalar_multiply_vector(vector *a, double scalar);
vector get_cross_product(vector *a, vector *b);