#pragma once

void read_data(char *file_name, raw_data *data, vector *sun_coords, double *ecliptical_inclination);
void write_orbit_elements_in_file(char *file_name, orbit_model *orbit, char *key);