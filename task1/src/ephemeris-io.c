#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ephemeris.h"

void _go_next_line(FILE *file, int count)
{
    char tmp;
    for (int i = 0; i < count; i++)
    {
        do
            tmp = fgetc(file);
        while (tmp != '\n');
    }
}

void _read_orbit_model(FILE *file, orbit_model *orbit)
{
    _go_next_line(file, 1);
    fscanf(file, "%lf", &orbit->semimajor_axis);
    fscanf(file, "%lf", &orbit->eccentricity);
    fscanf(file, "%lf", &orbit->inclination);
    fscanf(file, "%lf", &orbit->periapsis_argument);
    fscanf(file, "%lf", &orbit->node_longitude);
    fscanf(file, "%lf", &orbit->initial_mean_anomaly);
    fscanf(file, "%lf", &orbit->JD_day);
    _go_next_line(file, 2);
}

double _read_dd_mm_ss_format(FILE *file)
{
    int sign = 1;
    double seconds;
    double radian_angle;
    int integer, minutes;
    double convert_const = M_PI / 180;
    fscanf(file, "%3i:%2i:%lf", &integer, &minutes, &seconds);
    if (integer < 0)
        sign = -1;
    radian_angle = (integer + sign * (minutes / 60.0 + seconds / 3600.0)) * convert_const;
    return radian_angle;
}

void _read_sun_data_line(FILE *file, double *JD_day, vector *sun_coords, double *ecliptical_inclination)
{
    fscanf(file, "%lf %lf %lf %lf", JD_day, &(*sun_coords).x, &(*sun_coords).y, &(*sun_coords).z);
    *ecliptical_inclination = _read_dd_mm_ss_format(file);
}

void read_data(char *file_name, orbit_model *orbit, int *date_count, double **JD_days,
               vector **sun_coords, double **ecliptical_inclination_series)
{
    FILE *file;
    file = fopen(file_name, "r");
    if (file == NULL)
    {
        fprintf(stderr, "Can not open file %s. Aborted.\n", file_name);
        exit(EXIT_FAILURE);
    }

    _read_orbit_model(file, orbit);
    fscanf(file, "%i", date_count);
    _go_next_line(file, 2);

    *JD_days = malloc(sizeof(vector) * (*date_count));
    *sun_coords = malloc(sizeof(vector) * (*date_count));
    *ecliptical_inclination_series = malloc(sizeof(double) * (*date_count));
    for (int i = 0; i < *date_count; i++)
        _read_sun_data_line(file, &(*JD_days)[i], &(*sun_coords)[i], &(*ecliptical_inclination_series)[i]);

    fclose(file);
}

void _convert_angles(double convert_const, int series_size, double *angle_series)
{
    for (int i = 0; i < series_size; i++)
        angle_series[i] *= convert_const;
}

void _convert_fractional_part(double angle, int *integral_part, int *minutes, int *seconds)
{
    int sign_factor = 1;
    double tmp, fractional_part;
    if (angle < 0)
        sign_factor = -1;

    *integral_part = angle;
    fractional_part = modf(angle * sign_factor, &tmp) * 60.0;
    *minutes = fractional_part;
    *seconds = modf(fractional_part, &tmp) * 60.0;
}

void _write_coordinates_radian_format(FILE *file, int series_size, double *JD_days, 
                                      double *RA_series, double *DEC_series)
{
    for (int i = 0; i < series_size; i++)
        fprintf(file, "%lf  %lf  %lf\n", JD_days[i], RA_series[i], DEC_series[i]);
}

void _write_coordinates_dd_mm_ss_format(FILE *file, int series_size, double *JD_days,
                                        double *RA_series, double *DEC_series)
{
    int RA_int, RA_minute, RA_seconds;
    int DEC_int, DEC_minute, DEC_seconds;
    _convert_angles(12.0 / M_PI, series_size, RA_series);
    _convert_angles(180.0 / M_PI, series_size, DEC_series);
    for (int i = 0; i < series_size; i++)
    {
        _convert_fractional_part(RA_series[i], &RA_int, &RA_minute, &RA_seconds);
        _convert_fractional_part(DEC_series[i], &DEC_int, &DEC_minute, &DEC_seconds);
        fprintf(file, "%lf  ", JD_days[i]);
        fprintf(file, "%3i:%2i:%2i  ", RA_int, RA_minute, RA_seconds);
        fprintf(file, "%3i:%2i:%2i\n", DEC_int, DEC_minute, DEC_seconds);
    }
}

void write_coordinates_in_file(char *file_name, int series_size, double *JD_days,
                               double *RA_series, double *DEC_series, char *key)
{
    FILE *file;
    file = fopen(file_name, "w");
    if (file == NULL)
    {
        fprintf(stderr, "Can not open file %s. Aborted.\n", file_name);
        exit(EXIT_FAILURE);
    }
    fprintf(file, "# %i \n", series_size);

    const char *key1 = "rad";
    const char *key2 = "dd:mm:ss";

    if (strcmp(key, key1) == 0)
        _write_coordinates_radian_format(file, series_size, JD_days, RA_series, DEC_series);
    else if (strcmp(key, key2) == 0)
        _write_coordinates_dd_mm_ss_format(file, series_size, JD_days, RA_series, DEC_series);
    else
        printf("write_coordinates_in_file: key %s incorrect or not given. Writing interrupted.\n", key);

    fclose(file);
}